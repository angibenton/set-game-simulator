/** Object to represent a card in the game Set.
 *  A card is defined by 4 properties: color, shape, shading, and number, which are each represented by integers in this class.
 *  There are 3 choices for the 4 properties, so there are 3^4 = 81 total card variations.
 *  Color: Red = 1, Purple = 2, Green = 3
 *  Shape: Oval = 1, Diamond = 2, Squiggle = 3
 *  Shading: Outline = 1, Lined = 2, Solid = 3
 *  Number: 1, 2 or 3
 *  example: a card with properties {3, 1, 2, 2} would look like 2 ovals that are lined in green
 */

/** Constructor for Card object
 *
 * @param color Red = 1, Purple = 2, Green = 3
 * @param shape Oval = 1, Diamond = 2, Squiggle = 3
 * @param shading Outline = 1, Lined = 2, Solid = 3
 * @param number 1, 2 or 3
 */
function Card (color, shape, shading, number) {

    if (color !== 1 || color !== 2 || color !== 3) {
        throw new Error("Card's color value must be 1, 2 or 3");
    }

    this.color = color; //1 = red, 2 = purple, 3 = green
    this.shape = shape; //1 = oval 2 = diamond, 3 = squiggle
    this.shading = shading; //1 = outline, 2 = lined, 3 = solid
    this.number = number; //1, 2, 3

    //ID PROPERTY?

}


    /** to String method for debugging
     *
     * @returns {string} a string representation of the given card
     */
    Card.prototype.toString = function() {
        let string = "Card: \n";
        string += "color: " + this.getColorString() + "\n";
        string += "shape: " + this.getShapeString() + "\n";
        string += "shading: " + this.getShadingString() + "\n";
        string += "number: " + this.getNumberString() + "\n";
        return string;
    };


    /** String representation of the color for the given card
     *
     * @returns {string} identifier for the color
     */
    Card.prototype.getColorString = function() {
        switch(this.color){
            case 1:
                return "RED";
            case 2:
                return "PURPLE";
            case 3:
                return "GREEN";
            default:
                throw new Error("Card's color value must be 1, 2 or 3");
        }
    };


    /** String representation of the shape for the given card
     *
     * @returns {string} identifier for the shape
     */

    Card.prototype.getShapeString = function (){
        switch(this.shape){
            case 1:
                return "OVAL";
            case 2:
                return "DIAMOND";
            case 3:
                return "SQUIGGLE";
            default:
                throw new Error("Card's shape value must be 1, 2 or 3");
        }
    };

    /** String representation of the shading for the given card
     *
     * @returns {string} identifier for the shading
     */
    Card.prototype.getShadingString = function (){
        switch(this.shading){
            case 1:
                return "OUTLINE";
            case 2:
                return "LINED";
            case 3:
                return "SOLID";
            default:
                throw new Error("Card's shading value must be 1, 2 or 3");
        }
    };

    /** String representation of the number for the given card
     *
     * @returns {string} identifier for the number
     */
    Card.prototype.getNumberString = function (){
        switch(this.number){
            case 1:
                return "1";
            case 2:
                return "2";
            case 3:
                return "3";
            default:
                throw new Error("Card's number value must be 1, 2 or 3");
        }
    };


    Card.prototype.getRGBcode = function (){
        switch(this.color) {
            case 1:
                return [255, 0, 0]; //RED
            case 2:
                return [97, 0, 153]; //PURPLE
            case 3:
                return [12, 140, 0]; //GREEN
            default:
                throw new Error("Card's color value must be 1, 2 or 3");
        }
    };

    /**determines if 3 cards form a set.
     * definition: 3 cards are a set if, for some property, they are either IDENTICAL or ALL DIFFERENT.
     * In other words, there cannot be 2 cards that are more like each other than they each are like the third.
     * example: (Red Oval Lined 1), (Red Diamond Lined 2), (Red Squiggle Lined 3) are a set.
     * @param card1
     * @param card2
     * @param card3
     * @return boolean true if set, false if not set
     */
     function set(card1, card2, card3){
        let colorGood =  checkSingleProperty(card1.color, card2.color, card3.color);
        let shapeGood =  checkSingleProperty(card1.shape, card2.shape, card3.shape);
        let shadingGood = checkSingleProperty(card1.shading, card2.shading, card3.shading);
        let numberGood =  checkSingleProperty(card1.number, card2.number, card3.number);
        return colorGood && shapeGood && shadingGood && numberGood;
    }

    /** Determine if a single attribute (encoded by integers) is ok for a set.
     * i.e., that the ints (1 2 or 3) are either all different or all the same
     * It works out that this can be determined by their sum. The sum will be divisible by
     * 3 if the numbers are 123 111 222 or 333, but not if they are 112 113 221 223 331 or 332
     * @param int1
     * @param int2
     * @param int3
     * @return boolean true if all same or all different, false otherwise
     */
    function  checkSingleProperty(int1, int2, int3){
        let sum = int1 + int2 + int3;
        return (sum % 3 === 0);
    }






